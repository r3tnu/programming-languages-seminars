; print_hex.asm
section .data
codes:
    db      '0123456789ABCDEF'

section .text

global exit
global print_hex
global print_newline
global print_char

exit:
    mov rax, 60
    xor rdi, rdi
    syscall

print_hex:
    push rdi
    mov rax, rsi
    mov rdi, 4
    mul rdi
    mov rcx, rax

    mov  rdi, 1
    mov  rdx, 1
    pop rax
	; Each 4 bits should be output as one hexadecimal digit
	; Use shift and bitwise AND to isolate them
	; the result is the offset in 'codes' array
.loop:
    push rax
    sub  rcx, 4
	; cl is a register, smallest part of rcx
	; rax -- eax -- ax -- ah + al
	; rcx -- ecx -- cx -- ch + cl
    sar  rax, cl
    and  rax, 0xf

    lea  rsi, [codes + rax]
    mov  rax, 1

    ; syscall leaves rcx and r11 changed
    push rcx
    syscall
    pop  rcx

    pop rax
	; test can be used for the fastest 'is it a zero?' check
	; see docs for 'test' command
    test rcx, rcx
    jnz .loop
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA

; Принимает код символа и выводит его в stdout
print_char:
    push rdi            ;saving the char to output in stack

    mov rsi, rsp        ;passing the stack address as the write argument
    mov rdx, 1          ;length
    mov rax, 1
    mov rdi, 1
    syscall

    pop rdi             ;fixing stack
    ret
