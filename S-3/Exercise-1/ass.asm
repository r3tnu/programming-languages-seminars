%macro allocate_memory 1+
  section .data
    str: db %1
%endmacro

allocate_memory "hello", "another", "word"

section .text

global _start

_start:

	mov rax, 60
	xor rdi, rdi
	syscall

