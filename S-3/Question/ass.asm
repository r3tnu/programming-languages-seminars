%define process(x) (x * 3 + 23)

section .text

global _start

_start:
	mov rax, process(rcx)

	mov rax, 60
	xor rdi, rdi
	syscall
