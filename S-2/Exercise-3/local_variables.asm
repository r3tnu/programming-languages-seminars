%include "lib.inc"

section .text

global _start

local_variables:
    sub rsp, 8
    mov byte[rsp], 0xAA
    mov byte[rsp+2],0xBB
    mov byte[rsp+4], 0xFF

    mov rdi, [rsp]
    mov rsi, 2
    call print_hex
    call print_newline

    mov rdi, [rsp+2]
    mov rsi, 2
    call print_hex
    call print_newline

    mov rdi, [rsp+4]
    mov rsi, 2
    call print_hex
    call print_newline

    add rsp, 8
    ret

_start:
    call local_variables
    call exit

