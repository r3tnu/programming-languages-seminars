

section .text



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r10, r10

    .loop:
        mov r10b, byte[rdi + rdx]   ;get the next char
        sub r10b, '0'               ;offset it by the ASCII code of 0 so we get the actual digit
        cmp r10b, 9                 ;check if it is a digit
        ja .end                     ;if not digit return

        imul rax, rax, 10           ;offset the resulting number by ten to then
        add rax, r10                ;add the next number in a smaller unit place

        inc rdx                     ;increase the counter
    jmp .loop

    .end:
    ret
