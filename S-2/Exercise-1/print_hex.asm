; print_hex.asm
section .text
%include "lib.inc"

global _start

_start:
    ; number 1122... in hexadecimal format
    mov  rdi, 0x11
    mov rsi, 2
    call print_hex
    call print_newline

    mov rdi, 0x22
    mov rsi, 2
    call print_hex
    call print_newline

    mov rdi, 0x33
    mov rsi, 2
    call print_hex
    call print_newline

    call exit
