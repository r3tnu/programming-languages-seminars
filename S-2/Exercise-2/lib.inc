section .text
exit:
    mov  rax, 60
    xor  rdi, rdi
    syscall

string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

print_string:
    push rdi            ;saving the passed string in stack
    call string_length
    pop rdi             ;getting the string back from stack

    mov rsi, rdi        ;string
    mov rdx, rax        ;length
    mov rax, 1          ;write syscall descr
    mov rdi, 1          ;stdout
    syscall

    ret
