- Какую оболочку используете вы?
  - Bash
- Как поменять оболочку, которая используется по умолчанию?
  - chsh -s <shell-path>
    - e.g. /bin/zsh
- Чем особенна оболочка sh и почему мы редко ее используем?
  - Оболочка sh была изначально разработана для скриптового программирования и автоматизации задач в командной строке, поэтому не обладает удобствами (TAB завершение, горячие клавиши, просмотр истории команд), которыми обладают современные оболочки)
  - sh используется в скриптовом программировании, а также она может использоваться для поддержания совместимости
- Как вывести результат выполнения команды в файл?
  - echo 'test' > file
- Что такое дерево процессов?
  - Это иерархическая структура, отображающая отношения процессов в ОС.
- Что такое `stdin` `stdout` `stderr`?
  - Стандартные потоки ввода-вывода
  - Можно "ловить" `stdout` и `stderr` при помощи `>` `>>` и `2>` `2>>` соответственно
- Как соединить выход одной программы со входом другой?
  - При помощи pipeline
    - echo 'test' | grep 'e'
- Когда целесообразно использовать `stdout`, а когда — `stderr`?
  - stout целесообразно использовать для вывода нормальных результатов работы программы в понятном для пользователя виде.
  - stderr целесообразно использовать для вывода ошибок и диагностической информации



Переменные наследются для этого и используется `export` 

Флаг `-g` в `nasm` генерирует debug информацию

Программа для автомотизации процесса отладки (поиска ошибок и unintended beheiviour)

`XOR` регистра с самим собой обнуляет его



Sections могут быть релоцированны и разделяют программу на секции

| Section Name           | Description                                          |
| ---------------------- | ---------------------------------------------------- |
| ".bss"                 | Uninitialized read-write data.                       |
| ".comment"             | Version control information.                         |
| ".data" & ".data1"     | Initialized read-write data.                         |
| ".debug"               | Debugging information.                               |
| ".fini"                | Runtime finalization instructions.                   |
| ".init"                | Runtime initialization instructions.                 |
| ".rodata" & ".rodata1" | Read-only data.                                      |
| ".text"                | Executable instructions.                             |
| ".line"                | Line # info for symbolic debugging.                  |
| ".note"                | Special information from vendors or system builders. |

Чтобы не использовать числовые значения адресов в программе используются `label`ы, которые по факту являются ссылками на адрес

### Состояние программы

В следующих двух командах используется обозначение `/FMT` для указания формата данных.

- `print /FMT <val>` позволяет посмотреть содержимое регистров или памяти. Регистры предваряются долларом, например, `print /x $rax`.

- ```
  x /FMT <address>
  ```

   позволяет смотреть содержимое памяти. Он отличается от 

  ```
  print
  ```

   тем, что принимает адрес, т.е. имеет один уровень косвенности.    

  `/FMT` позволяет нам явно указывать тип данных, который мы изучаем. В зависимости от него меняются две вещи:

  - формат отображения (например, основание системы счисления);

  - ожидаемый размер.        

    Например, в памяти лежит двоичное число `0xff78`. Если мы считаем 1 байт равный `0x78`, то это число мы интерпретируем как положительное; если же мы считаем 2 байта, то оно может быть интерпретировано как отрицательное (почему?)

Самые полезные форматы:

- `x` (hexadecimal)
- `a` (address)
- `i` (instruction, `gdb` попытается дизассемблировать инструкцию начиная с данного адреса)
- `c` (char)
- `s` (null-terminated string) – строка, в которой каждый байт соответствует    коду символа по таблице ASCII, а конец строки обозначен символом с кодом    `0x00`.

Самые полезные размеры: `b` (байт) `g` (giant, 8 байт).



Name Alias Description
r0 rax Kind of an “accumulator,” used in arithmetic instructions. For example, an instruction
div is used to divide two integers. It accepts one operand and uses rax implicitly as
the second one. After executing div rcx a big 128-bit wide number, stored in parts in
two registers rdx and rax is divided by rcx and the result is stored again in rax.
r3 rbx Base register. Was used for base addressing in early processor models.
r1 rcx Used for cycles (e.g., in loop).
r2 rdx Stores data during input/output operations.
r4 rsp Stores the address of the topmost element in the hardware stack. See section 1.5
“Hardware stack”.
r5 rbp Stack frame’s base. See section 14.1.2 “Calling convention”.
r6 rsi Source index in string manipulation commands (such as movsd)
r7 rdi Destination index in string manipulation commands (such as movsd)
r8
r9 … r15 no Appeared later. Used mostly to store temporal variables (but sometimes used
implicitly, like r10, which saves the CPU flags when syscall instruction is
executed. See Chapter 6 “Interrupts and system calls”)

