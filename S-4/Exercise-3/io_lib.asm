; hello_mmap.asm
%define O_RDONLY 0 
%define PROT_READ 0x1
%define MAP_PRIVATE 0x2
%define SYS_WRITE 1
%define SYS_OPEN 2
%define SYS_MMAP 9
%define SYS_EXIT 60
%define SYS_FSTAT 5
%define SYS_CLOSE 3
%define SYS_MUNMAP 11
%define FD_STDOUT 1


section .text

global print_file
global print_string

; use exit system call to shut down correctly
exit:
    mov  rax, SYS_EXIT
    xor  rdi, rdi
    syscall

; These functions are used to print a null terminated string
; rdi holds a string pointer
print_string:
    push rdi
    call string_length
    pop  rsi
    mov  rdx, rax 
    mov  rax, SYS_WRITE
    mov  rdi, FD_STDOUT
    syscall
    ret

string_length:
    xor  rax, rax
.loop:
    cmp  byte [rdi+rax], 0
    je   .end 
    inc  rax
    jmp .loop 
.end:
    ret

; This function is used to print a substring with given length
; rdi holds a string pointer
; rsi holds a substring length
print_substring:
    mov  rdx, rsi 
    mov  rsi, rdi
    mov  rax, SYS_WRITE
    mov  rdi, FD_STDOUT
    syscall
    ret

; in ; ;rdi = fname; ;rsi = flags;
; out ; ;rax = file descr;
open:
    mov rax, SYS_OPEN
    mov rdx, 0
    syscall
    ret

; in ; ;rdi = file descriptor;
close:
    mov rax, SYS_CLOSE
    syscall
    ret

; in ; ;rdi = file descriptor; ;rsi = buffer address;
fstat:
    mov rax, SYS_FSTAT
    syscall
    ret

; in ; ;rdi=filedesc; ;rsi=length;
; out ; ;rax=pointer;
mmap:
    mov r8, rdi
    mov rax, SYS_MMAP
    mov rdi, 0
    mov rdx, PROT_READ
    mov r10, MAP_PRIVATE
    mov r9, 0
    syscall
    ret

; in ; ;rdi=pointer; ;rsi=length;
munmap:
    mov rax, SYS_MUNMAP
    syscall
    ret


; in ; ;rdi = filename;
print_file:
    ;initialing rbp
    push rbp
    mov rbp, rsp

    sub rsp, 8          ;for file descr
    sub rsp, 8          ;for length
    sub rsp, 8          ;for pointer to mapped file


    ;Opening file
    mov  rsi, O_RDONLY
    call open
    mov qword[rbp-8], rax                ;file descriptor

    ;Running fstat
    sub rsp, 144

    mov rdi, [rbp-8]
    mov rsi, rsp
    call fstat

    mov rax, qword[rsp+48]
    mov qword[rbp-16], rax
    add rsp, 144


    ;Running mmap
    mov rdi, qword[rbp-8]
    mov rsi, qword[rbp-16]
    call mmap
    mov qword[rbp-24], rax

    ;Closing the file after we've mapped it
    mov rdi, qword[rbp-8]
    call close

    ;Prining file contents
    mov rdi, qword[rbp-24]
    call print_string


    ;Running munmap
    mov rdi, qword[rbp-24]
    mov rsi, qword[rbp-16]
    call munmap

    add rsp, 24
    pop rbp
    call exit