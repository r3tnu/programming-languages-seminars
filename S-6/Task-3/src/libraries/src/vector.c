#include "../headers/vector.h"

#include <stddef.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

static const size_t DEFAULT_CAPACITY = 100;
static const int64_t DEFAULT_VALUE = 0;

struct vector {
    size_t capacity;
    size_t size;
    int64_t* data;
};

/*----------ELEMENT ACCESS----------*/

int64_t* vector_at(const struct vector* vector, size_t at) {
    if (!vector) return NULL;
    if (at >= vector->size) return NULL;
    return &vector->data[at];
}

int64_t* vector_front(const struct vector* vector) {
    if (!vector) return NULL;
    if (vector->size == 0) return NULL;
    return &vector->data[0];
}

int64_t* vector_back(const struct vector* vector) {
    if (!vector) return NULL;
    if (vector->size == 0) return NULL;
    return &vector->data[vector->size-1];
}

/*----------CAPACITY----------*/

bool vector_extend(struct vector* vector) {
    if (!vector) return false;
    int64_t* new_data = realloc(vector->data, sizeof(int64_t)  * (vector->capacity + DEFAULT_CAPACITY));
    if (new_data) {
        vector->capacity = vector->capacity + DEFAULT_CAPACITY;
        vector->data = new_data;
        return true;
    }
    return false;
}

bool vector_extend_by_capacity(struct vector* vector, size_t added_capacity) {
    if (!vector) return false;
    if (added_capacity == 0) return true;
    int64_t* new_data = realloc(vector->data, sizeof(int64_t) * (vector->capacity + added_capacity));
    if (new_data) {
        vector->capacity = vector->capacity + added_capacity;
        vector->data = new_data;
        return true;
    }
    return false;
}

bool vector_reserve(struct vector* vector, size_t new_capacity) {
    if (!vector) return false;
    if (vector->size > new_capacity) return false;

    int64_t* new_data = realloc(vector->data, sizeof(int64_t) * new_capacity);
    if (new_data) {
        vector->capacity = new_capacity;
        vector->data = new_data;
        return true;
    }
    return false;
}

bool vector_shrink_to_fit(struct vector* vector) {
    if (!vector) return false;
    int64_t* new_data = realloc(vector->data, sizeof(int64_t) * vector->size);
    if (new_data) {
        vector->capacity = vector->size;
        vector->data = new_data;
        return true;
    }
    return false;
}

void vector_free(struct vector* vector) {
    if (!vector) return;
    free(vector->data);
    free(vector);
}

size_t vector_size(const struct vector* vector) {
    if (!vector) return -1;
    return vector->size;
}

size_t vector_capacity(const struct vector* vector) {
    if (!vector) return -1;
    return vector->capacity;
}

int64_t* vector_get_raw_data_copy(const struct vector* vector) {
    if (!vector) return NULL;
    if (vector->size == 0) return NULL;

    int64_t* data_copy = malloc(sizeof(int64_t) * vector->size);
    for (size_t i = 0; i < vector->size; i++) {
        data_copy[i] = *vector_at(vector, i);
    }
    return data_copy;
}

/*----------MODIFIERS----------*/

bool vector_insert(const struct vector* vector, size_t at, int64_t value) {
    if (!vector) return false;
    if (at >= vector->size) return false;
    vector->data[at] = value;
    return true;
}

bool vector_push_back(struct vector* vector, int64_t element) {
    if (!vector) return false;
    if (vector->capacity == vector->size) {
        if(!vector_extend(vector)) {
            return false;
        }
    }
    vector->data[vector->size] = element;
    vector->size += 1;
    return true;
}

bool vector_push_back_array(struct vector* vector, const int64_t* array, size_t array_size) {
    if (!vector) return false;
    if (vector->capacity < vector->size + array_size) {
        if(!vector_extend_by_capacity(vector, array_size + DEFAULT_CAPACITY)) {
            return false;
        }
    }

    for (size_t i = 0; i < array_size; i++) {
        vector_push_back(vector, array[i]);
    }
    return true;
}

bool vector_pop_back(struct vector* vector) {
    if (!vector) return false;
    if (vector->size == 0) return false;
    vector->size -= 1;
    return true;
}

bool vector_resize(struct vector* vector, size_t new_size) {
    if (!vector) return false;
    if (new_size > vector->capacity) {
        if(!vector_reserve(vector, new_size + DEFAULT_CAPACITY)) {
            return false;
        }
    }

    if (new_size > vector->size) {
        for (size_t i = 0; i < new_size - vector->size; i++) {
            vector_push_back(vector, DEFAULT_VALUE);
        }
        return true;
    }

    vector->size = new_size;
    return true;


}

void vector_clear(struct vector* vector) {
    if (!vector) return;
    vector->size = 0;
}

/*----------CONSTRUCTORS----------*/

struct vector* vector_create() {
    struct vector* vector = malloc(sizeof(struct vector));
    vector->capacity = DEFAULT_CAPACITY;
    vector->data = malloc(sizeof(int64_t) * DEFAULT_CAPACITY);
    vector->size = 0;
    return vector;
}

struct vector* vector_create_with_capacity(size_t capacity) {
    struct vector* vector = malloc(sizeof(struct vector));
    vector->capacity = DEFAULT_CAPACITY;
    vector->data = malloc(sizeof(int64_t) * capacity);
    vector->size = 0;
    return vector;
}

struct vector* vector_create_from_array(const int64_t* array, size_t array_size) {
    struct vector* vector = vector_create_with_capacity(array_size + DEFAULT_CAPACITY);
    vector_push_back_array(vector, array, array_size);
    return vector;
}

struct vector* vector_create_from_vector(const struct vector* vector) { //Does putting const guarantee that we do not touch the vector.data?
    struct vector* copy_vector = vector_create_with_capacity(vector_capacity(vector));
    for (size_t i = 0; i < vector_size(vector); i++) {
        vector_push_back(copy_vector, *vector_at(vector, i));
    }
    return copy_vector;
}

/*----------OUTPUT----------*/

void vector_out(const struct vector* vector, FILE* out) {
    fprintf(out, "[ ");
    for(size_t i = 0; i < vector->size-1; i++) {
        fprintf(out, "%" PRId64 ", ", *vector_at(vector, i));
    }
    fprintf(out, "%" PRId64 " ]\n", *vector_at(vector, vector->size-1));
}

void vector_out_all(const struct vector* vector, FILE* out) {
    fprintf(out, "| SIZE: %zu | CAPACITY: %zu |\n", vector_size(vector), vector_capacity(vector));
    vector_out(vector, out);
}