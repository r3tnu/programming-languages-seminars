#ifndef VECTOR_H
#define VECTOR_H

#include <stddef.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

struct vector;

int64_t* vector_at(const struct vector* vector, size_t at);
int64_t* vector_front(const struct vector* vector);
int64_t* vector_back(const struct vector* vector);

bool vector_extend(struct vector* vector);
bool vector_extend_by_capacity(struct vector* vector, size_t added_capacity);
bool vector_reserve(struct vector* vector, size_t new_capacity);
bool vector_shrink_to_fit(struct vector* vector);
void vector_free(struct vector* vector);
size_t vector_size(const struct vector* vector);
size_t vector_capacity(const struct vector* vector);
int64_t* vector_get_raw_data_copy(const struct vector* vector);

bool vector_insert(const struct vector* vector, size_t at, int64_t value);
bool vector_push_back(struct vector* vector, int64_t element);
bool vector_push_back_array(struct vector* vector, const int64_t* array, size_t array_size);
bool vector_pop_back(struct vector* vector);
bool vector_resize(struct vector* vector, size_t new_size);

struct vector* vector_create();
struct vector* vector_create_with_capacity(size_t capacity);
struct vector* vector_create_from_array(const int64_t* array, size_t array_size);
struct vector* vector_create_from_vector(const struct vector* vector);

void vector_out(const struct vector* vector, FILE* out);
void vector_out_all(const struct vector* vector, FILE* out);

#endif