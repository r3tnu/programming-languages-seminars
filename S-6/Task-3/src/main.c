#include "libraries/headers/vector.h"

#include <stdio.h>

int main() {
    struct vector* vector = vector_create();

    for(int64_t i = 0; i < 101; i++) {
        vector_push_back(vector, i);
    }
    vector_out_all(vector, stdout);

    printf("%" PRId64 " %" PRId64 " %" PRId64 "\n", *vector_at(vector, 25), *vector_front(vector), *vector_back(vector));

    vector_extend(vector);
    vector_out_all(vector, stdout);

    vector_extend_by_capacity(vector, 10);
    vector_out_all(vector, stdout);

    vector_reserve(vector, 125);
    vector_out_all(vector, stdout);

    vector_shrink_to_fit(vector);
    vector_out_all(vector, stdout);

    vector_insert(vector, 5, 255);
    vector_out_all(vector, stdout);

    int64_t arr[] = {255, 255, 255};
    vector_push_back_array(vector, arr, 3);
    vector_out_all(vector, stdout);

    vector_pop_back(vector);
    vector_out_all(vector, stdout);

    vector_resize(vector, 10);
    vector_out_all(vector, stdout);

    vector_free(vector);
}