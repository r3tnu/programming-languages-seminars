#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>

// Macros for printing different types
#define _print(type, x) type##_print(x)

void int64_t_print(int64_t i) { printf("%" PRId64, i); }
void double_print(double d) { printf("%lf", d); }
void newline_print() { puts(""); }

//Macros for defining a list of a certain type
#define DEFINE_LIST(type)                                               \
  struct list_##type {                                                  \
    type value;                                                         \
    struct list_##type* next;                                           \
  };                                                                    \
                                                                        \
	void list_##type##_push( struct list_##type** old_l, type i ) {       \
		if ( *old_l == NULL ) {                                             \
			*old_l = malloc( sizeof( struct list_##type ) );                  \
			(*old_l)->value = i;                                              \
			return;                                                           \
		}                                                                   \
		struct list_##type* l = *old_l;                                     \
		while( l->next != NULL ) {                                          \
			l = l->next;                                                      \
		}                                                                   \
		l->next = malloc( sizeof( struct list_##type ) );                   \
		l->next->value = i;                                                 \
	}                                                                     \
                                                                        \
	void list_##type##_print( const struct list_##type* l ) {             \
		while( l != NULL ) {                                                \
			_print( type, l->value );                                         \
			printf( " " );                                                    \
			l = l->next;                                                      \
		}                                                                   \
		_print( newline, "" );                                              \
	}                                                                     

DEFINE_LIST(int64_t)
DEFINE_LIST(double)

int main() {
	
	struct list_double* l = NULL;

	list_double_print( l );

	list_double_push( &l, 12.0 );

	list_double_print( l );

	list_double_push( &l, 12.0 );
	list_double_push( &l, 12.0 );
	list_double_push( &l, 12.0 );
	list_double_push( &l, 12.0 );
	list_double_print( l );

	return 0;
}
