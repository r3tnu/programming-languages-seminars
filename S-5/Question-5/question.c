#include <stdio.h>

#define print_var(x) printf(#x " is %d", x )

int main() {

	print_var( 42 );
	printf("\n");

	return 0;
}
