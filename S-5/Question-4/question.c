#include <stdio.h>

// Добавили скобки у "y", чтобы даже если в "y" лежит выражние, именно оно умножалось на 2
#define dbl( y ) ( y ) * 2

int main() {
	printf( "%d", dbl( 3 + 3 ) );
	return 0;
}
