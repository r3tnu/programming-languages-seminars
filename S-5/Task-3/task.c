#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>

// Macros for printing different types
#define _print(type, x) type##_print(x)

void int64_t_print(int64_t i) { printf("%" PRId64, i); }
void double_print(double d) { printf("%lf", d); }
void newline_print() { puts(""); }

//Macros for defining a list of a certain type
#define DEFINE_LIST(type)                                               \
  struct list_##type {                                                  \
    type value;                                                         \
    struct list_##type* next;                                           \
  };                                                                    \

void error(const char *s) {
  fprintf(stderr, "%s", s);
  abort();
}

DEFINE_LIST(int64_t)
DEFINE_LIST(double)

#define list_push( l, i ) _Generic( ( l ), \
		struct list_int64_t** : list_int64_t_push, \
		struct list_double** : list_double_push, \
		default : error("Unsupported operation") ) ( l, i )

void list_int64_t_push( struct list_int64_t** old_l, int64_t i ) {       
		if ( *old_l == NULL ) {                                             
			*old_l = malloc( sizeof( struct list_int64_t ) );                  
			(*old_l)->value = i;                                              
			return;                                                           
		}                                                                   
		struct list_int64_t* l = *old_l;                                     
		while( l->next != NULL ) {                                          
			l = l->next;                                                     
		}                                                                  
		l->next = malloc( sizeof( struct list_int64_t ) );
		l->next->value = i;                             
	}

void list_double_push( struct list_double** old_l, double i ) {       
		if ( *old_l == NULL ) {                                             
			*old_l = malloc( sizeof( struct list_double ) );                  
			(*old_l)->value = i;                                              
			return;                                                           
		}                                                                   
		struct list_double* l = *old_l;                                     
		while( l->next != NULL ) {                                          
			l = l->next;                                                     
		}                                                                  
		l->next = malloc( sizeof( struct list_double ) );
		l->next->value = i;                             
	}



#define list_print( l ) _Generic( ( l ), \
		struct list_int64_t* : list_int64_t_print, \
		struct list_double* : list_double_print, \
		default : error("Unsupported operation") ) ( l )

void list_int64_t_print( const struct list_int64_t* l ) {
	while( l != NULL ) {                                  
		_print( int64_t, l->value );                          
		printf( " " );                                    
		l = l->next;                                     
	}                                                 
	_print( newline, "" );                           
}

void list_double_print( const struct list_double* l ) {
	while( l != NULL ) {                                  
		_print( double, l->value );                          
		printf( " " );                                    
		l = l->next;                                     
	}                                                 
	_print( newline, "" );                           
}

int main() {
	
	struct list_int64_t* l = NULL;

	list_print( l );

	list_push( &l, 12 );

	list_print( l );

	list_push( &l, 12 );
	list_push( &l, 12 );
	list_push( &l, 12 );
	list_push( &l, 12 );
	list_print( l );

	return 0;
}
