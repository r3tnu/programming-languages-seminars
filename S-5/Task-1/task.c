#include <stdio.h>

#define print_var(x) printf(#x " is %d", x )

int main() {
	
	int x = 0;
	const int c_x = 0;

	print_var( x );
	printf("\n");
	print_var( c_x );
	printf("\n");
	print_var( 2 );
	printf("\n");

	return 0;
}
